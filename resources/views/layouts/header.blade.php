<header class="main-header">
    <!-- Logo -->
    <a href="/anasayfa" class="logo">
        <span class="logo-mini"><b>T</b>L</span>
        <span class="logo-lg"><b>Time</b>Lapse</span>
    </a>
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Menu</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="/panel/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                        <span class="hidden-xs">{{ $kullanici->email }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user-header">
                            <img src="/panel/dist/img/user2-160x160.jpg" class="img-circle">
                            <p>
                                {{ $kullanici->email }}
                                <small> <strong>Kayıt Tarihi: </strong> {{ date("d/m/Y  H:i:s", strtotime($kullanici->created_at)) }} </small>
                            </p>
                        </li>
                        <li class="user-footer">
                            {{--<div class="pull-left">--}}
                                {{--<a href="#" class="btn btn-default btn-flat"> <i class="fa fa-user"></i> Profil</a>--}}
                            {{--</div>--}}
                            {{--<div class="pull-left" style="margin-left: 8px">--}}
                                {{--<a href="#" class="btn btn-default btn-flat"> <i class="fa fa-lock"></i> Şifre Değiştir</a>--}}
                            {{--</div>--}}
                            <div class="pull-right">
                                <a href="{{ route('panel.cikis') }}" class="btn btn-danger btn-flat">Çıkış</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>