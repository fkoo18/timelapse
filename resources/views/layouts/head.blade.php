<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="stylesheet" href="/panel/bower_components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="/panel/bower_components/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="/panel/bower_components/Ionicons/css/ionicons.min.css">
<link rel="stylesheet" href="/panel/dist/css/AdminLTE.css">
<link rel="stylesheet" href="/panel/dist/css/skins/_all-skins.min.css">
<link rel="stylesheet" href="/panel/bower_components/morris.js/morris.css">
<link rel="stylesheet" href="/panel/bower_components/jvectormap/jquery-jvectormap.css">
<link rel="stylesheet" href="/panel/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
<link rel="stylesheet" href="/panel/bower_components/bootstrap-daterangepicker/daterangepicker.css">
<link rel="stylesheet" href="/panel/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<!-- META -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<style class="cp-pen-styles">
    .lightBox .img-bordered-sm:hover{
        border: 2px solid #000;
    }
</style>
<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@10.20.0/dist/lazyload.min.js"></script>