@extends("master")
@section("title","Anasayfa | TimeLapse")

@section("headerBaslik")
    <section class="content-header">
        <h1 class="hidden-xs">
            Tanımlı Cihazlar
            <small>Kontrol paneli</small>
            <button class="btn btn-success" onclick="window.location.reload()"> <i class="fa fa-refresh"></i>&nbsp; Yenile</button>
        </h1>
        <h1 style="font-size: 16px" class="visible-xs">
            Tanımlı Cihazlar2
            <small>Kontrol paneli</small><button class="btn btn-success pull-right" onclick="window.location.reload()" style="padding: 2px 10px;position: relative;top: -5px;"> <i class="fa fa-refresh"></i>&nbsp; Yenile</button>
        </h1>
    </section>
@endsection

@section("content")

    <div class="col-md-12">
        @foreach($ownedDevice as $item)
            {{ /* item burada Device Model'idir. */ ' '}}
            <div class="col-lg-1 col-md-3 col-xs-6 text-center imgResponsiveee {{ $item->device->onlineStatus == 1 ? 'online' : 'offline' }}">

                    <img width="100%" src="/panel/camera.png" >

                <div class="col-md-12 no-padding">
                    <span class="users-list-name">{{ $item->device->hashKey }}</span>
                    <span class="users-list-name"><button data-id="{{ $item->device->id }}" class="showLog new-label btn btn-primary" style="padding: 1px 11px;margin-top: 5px;">  Log Bilgileri</button></span>
                    <span class="users-list-name"><a href="/devicePhotos?deviceID={{ $item->device->id }}" class="new-label btn btn-success" style="padding: 1px 11px;margin-top: 5px;">  Resimlere Git</a></span>
                </div>
            </div>
        @endforeach

    </div>


@endsection

@section("footer")
    <!-- Modal form to add a post -->
    <div id="showLog" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Log Bilgileri</h4>
                </div>
                <div class="modal-body">
                    <table class="table table-hover text-center" id="logLar">
                        <thead>
                            <tr>
                                <th>İşlem Adı</th>
                                <th>Tarih</th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr></tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


    <script>
        $(document).on('click', '.showLog', function() {
            $('#showLog').modal('show');
            $.ajax({
                type: 'POST',
                url: 'logBilgiGetir',
                data: {
                    _token  :  $('meta[name="csrf-token"]').attr('content'),
                    id      :  $(this).attr('data-id')
                },
                success: function(data) {
                    $('#logLar tbody').empty().append('<tr></tr>');
                    data.data.forEach(function (item) {
                        $('#logLar tbody tr:last').after(`<tr>
                            <td>`+ item.logDetail +`</td>
                            <td>`+ item.created_at +`</td>
                        </tr>`);
                    });
                }
            });
        });
    </script>


@endsection