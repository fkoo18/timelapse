@php
    use Illuminate\Support\Facades\Input;
@endphp
@extends("master")
@section("title","Cihaz Fotoğrafları | TimeLapse")

@section("head")
    <link rel="stylesheet" href="/panel/bower_components/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="/panel/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <style>
        /* lightBox */
        .row > .column {
            padding: 0 8px;
        }

        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        /* Create four equal columns that floats next to eachother */
        .column {
            float: left;
            width: 25%;
        }

        /* The Modal (background) */
        .modal {
            display: none;
            position: fixed;
            z-index: 10;
            padding-top: 100px;
            left: 0;
            top: 40px;
            width: 100%;
            height: 100%;
            overflow: auto;
            background-color: black;
        }

        /* Modal Content */
        .modal-content {
            position: relative;
            background-color: #fefefe;
            margin: auto;
            padding: 0;
            width: 90%;
            max-width: 900px;
        }

        /* The Close Button */
        .close {
            color: white;
            position: absolute;
            top: 60px;
            right: 30px;
            font-size: 40px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus {
            color: #999;
            text-decoration: none;
            cursor: pointer;
        }

        /* Hide the slides by default */
        .mySlides {
            display: none;
        }

        /* Next & previous buttons */
        .prev,
        .next {
            cursor: pointer;
            position: absolute;
            top: 50%;
            width: auto;
            padding: 16px;
            margin-top: -50px;
            color: white;
            font-weight: bold;
            font-size: 20px;
            transition: 0.6s ease;
            border-radius: 0 3px 3px 0;
            user-select: none;
            -webkit-user-select: none;
        }

        /* Position the "next button" to the right */
        .next {
            right: 0;
            border-radius: 3px 0 0 3px;
        }

        /* On hover, add a black background color with a little bit see-through */
        .prev:hover,
        .next:hover {
            background-color: rgba(0, 0, 0, 0.8);
        }

        /* Number text (1/3 etc) */
        .numbertext {
            color: #f2f2f2;
            font-size: 12px;
            padding: 8px 12px;
            position: absolute;
            top: 0;
        }

        /* Caption text */
        .caption-container {
            text-align: center;
            background-color: black;
            padding: 6px 16px;
            color: white;
            font-size: 16px;
        }

        img.demo {
            opacity: 0.6;
            border:2px solid transparent;
        }

        img.demo.active {
            border:2px solid white;
        }

        .active,
        .demo:hover {
            opacity: 1;
        }

        img.hover-shadow {
            transition: 0.3s;
        }

        .hover-shadow:hover {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }
        @media (max-width: 1366px) {
            .modal{
                top:10px;
            }
            .modal-content{
                max-width: 690px;
            }
        }
    </style>
@endsection

@section("headerBaslik")
    <section class="content-header">
        <h1>
            {{ $device->id }}
            <small>Bilgileri</small>
        </h1>
        <h1 style="margin: 5px 0 20px 0" class="text-center"> {{ date("d/m/Y", strtotime( request('baslangicTarih') ? request('baslangicTarih') : now() )) . " - " . date("d/m/Y", strtotime( request('baslangicTarih') ? request('bitisTarih') : now() )) }} Arası Fotoğraflar </h1>
    </section>
@endsection

@section("content")

    <div class="row hidden-print" style="margin-bottom: 10px;">

        <div class="col-lg-12 col-xs-12" style="margin-bottom: 10px;">

            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs pull-right">
                    <li class="active">
                        <a href="#search-calender" data-toggle="tab" aria-expanded="true">Dönemsel</a>
                    </li>
                    <li class="pull-left header"><i class="fa fa-inbox"></i>
                        Filtre
                    </li>
                </ul>
                <div class="tab-content padding-left-lg">
                    <div class="chart tab-pane active" id="search-calender" style="position: relative;">

                        <div class="col-lg-12 col-xs-12" style="margin-top: 10px;">
                            <label style="width: 65px;">Tarih Seç : </label>
                            <button type="button" class="btn btn-default" id="daterange-btn">
                                <span>
                                    <i id="daterange-label" class="fa fa-calendar"><span> TARİH </span></i>
                                </span>
                                <i class="fa fa-caret-down"></i>
                            </button>
                        </div>

                        <div class="col-lg-12 col-xs-12">
                            <div class="row" style="margin-top: 10px;">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Filter -->

    <style>
        .blur-up {
            -webkit-filter: blur(5px);
            filter: blur(5px);
            transition: filter 100ms, -webkit-filter 100ms;
        }

        .blur-up.loaded {
            -webkit-filter: blur(0);
            filter: blur(0);
        }
        #cihazResimler {
            overflow-y: scroll;
            -webkit-overflow-scrolling: touch;
        }
    </style>

    @php $i = 1; @endphp
    <div class="row" id="cihazResimler">
        @foreach($devicePhoto as $item)
            <div class="col-lg-2 lightBox">
                <span style="cursor: pointer;" onclick="openModal();currentSlide({{ $i }})">
                    <img class="img-bordered-sm thumbnail lazy blur-up" data-src="https://storage.googleapis.com/timelapsetr/{{ md5($device->hashKey) }}/{{ $item->photoName }}" width="100%">
                </span>
            </div>
            @php $i++; @endphp
        @endforeach
    </div>

    <!-- The Modal/Lightbox -->
    <div id="myModal" class="modal">
        <span class="close cursor" style="opacity: .6;" onclick="closeModal()">&times;</span>
        <div class="modal-content">

            @php $i = 1; @endphp
            @foreach($devicePhoto as $item)
                <div class="mySlides">
                    <div class="numbertext">{{ $i }} / {{ count($devicePhoto) }}</div>
                    <img class="lazy" src="https://storage.googleapis.com/timelapsetr/{{ md5($device->hashKey) }}/{{ $item->photoName }}" data-src="https://storage.googleapis.com/timelapsetr/{{ md5($device->hashKey) }}/{{ $item->photoName }}" style="width:100%">
                </div>
                @php $i++; @endphp
            @endforeach

            <!-- Next/previous controls -->
            <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
            <a class="next" onclick="plusSlides(1)">&#10095;</a>

            <!-- Caption text -->
            <div class="caption-container">
                <p id="caption"></p>
            </div>
            <!-- Thumbnail image controls -->

            @php $i = 1; @endphp
            @foreach($devicePhoto as $item)
                <div class="col-md-2 col-xs-4" style="margin:5px 0">
                    <img width="100%" style="cursor: pointer;" class="demo lazy" src="https://storage.googleapis.com/timelapsetr/{{ md5($device->hashKey) }}/{{ $item->photoName }}" data-src="https://storage.googleapis.com/timelapsetr/{{ md5($device->hashKey) }}/{{ $item->photoName }}" onclick="currentSlide({{ $i }})" alt="{{ date("d/m/Y  H:i:s", strtotime($item->date)) }}">
                </div>
                @php $i++; @endphp
            @endforeach

        </div>
    </div>

    <div class="text-center">
        {!! $devicePhoto->appends(Input::except('page')) !!}
    </div>


    <div id="lightbox" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <button type="button" class="close hidden" data-dismiss="modal" aria-hidden="true">×</button>
            <div class="modal-content">
                <div class="modal-body">
                    <img src="" alt="" />
                </div>
            </div>
        </div>
    </div>



@endsection

@section('footer')

    <script>
        var myLazyLoad = new LazyLoad({
            container: document.getElementById('cihazResimler')
        });
    </script>

    <!-- date-range-picker -->
    <script src="/panel/bower_components/moment/min/moment.min.js"></script>
    <script src="/panel/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script>

        $(document).ready(function () {

            var datepicker = $('#daterange-btn').daterangepicker(
                {
                    locale: {
                        "format": "DD/MM/YYYY",
                        "customRangeLabel": "Tarih Seç",
                        "applyLabel": "Uygula",
                        "cancelLabel": "İptal",
                        "daysOfWeek": [ "Paz", "Pts", "Sal", "Çar", "Per", "Cum", "Cts" ],
                        "monthNames": [ "Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"]
                    },
                    ranges: {
                        'Bugün': [],
                        'Dün': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Son 1 Hafta': [moment().subtract(6, 'days'), moment()],
                        'Son 1 Ay': [moment().subtract(31, 'days'), moment()],
                        'Bu Ay': [moment().startOf('month'), moment().endOf('month')],
                        'Önceki Ay': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment().subtract({{ !request('start') ? '0' : request('start') }}, 'days'),
                    endDate: moment().subtract({{ !request('finish') ? '0' : request('finish') }}, 'days')
                },
                function (start, end) {
                    var dateNow = new Date(moment().format('YYYY'), moment().format('MM'), moment().format('DD'));
                    var dateStart = new Date(start.format('YYYY'), start.format('MM'), start.format('DD'));
                    var dateEnd = new Date(end.format('YYYY'), end.format('MM'), end.format('DD'));
                    var oneDay = 24 * 60 * 60 * 1000;

                    var startSubtract = Math.abs((dateNow.getTime() - dateStart.getTime()) / (oneDay));
                    var endSubtract = Math.abs((dateNow.getTime() - dateEnd.getTime()) / (oneDay));

                    window.location.href = '?deviceID='+ <?php echo request('deviceID') ?> +'&baslangicTarih=' + start.format('YYYY-MM-DD') + '&bitisTarih=' + end.format('YYYY-MM-DD') + '&start=' + startSubtract + '&finish='+endSubtract;
                    $('#daterange-btn').html("<span> {{ date("d/m/Y", strtotime(request('baslangicTarih'))) }} - {{ date("d/m/Y", strtotime(request('bitisTarih'))) }} </span>");
                }
            );

            $('#daterange-label').html("<span" +
                "> @if(request('baslangicTarih')) {{ date("d/m/Y", strtotime(request('baslangicTarih'))) }} @else {{ date("d/m/Y", strtotime(now())) }}  @endif  - @if(request('baslangicTarih')) {{ date("d/m/Y", strtotime(request('bitisTarih'))) }} @else {{ date("d/m/Y", strtotime(now())) }}  @endif </span>");

        });

    </script>
    <script>
        $(document).ready(function() {
            var $lightbox = $('#lightbox');

            $('[data-target="#lightbox"]').on('click', function(event) {
                var $img = $(this).find('img'),
                    src = $img.attr('src'),
                    alt = $img.attr('alt'),
                    css = {
                        'maxWidth': $(window).width() - 100,
                        'maxHeight': $(window).height() - 100
                    };

                $lightbox.find('.close').addClass('hidden');
                $lightbox.find('img').attr('src', src);
                $lightbox.find('img').attr('alt', alt);
                $lightbox.find('img').css(css);
            });

            $lightbox.on('shown.bs.modal', function (e) {
                var $img = $lightbox.find('img');

                $lightbox.find('.modal-dialog').css({'width': $img.width()});
                $lightbox.find('.close').removeClass('hidden');
            });
        });
    </script>
    <script>
        // Open the Modal
        function openModal() {
            $( "#myModal" ).fadeIn( "slow" );
        }

        // Close the Modal
        function closeModal() {
            $( "#myModal" ).fadeOut( "slow" );
        }

        var slideIndex = 1;
        showSlides(slideIndex);

        // Next/previous controls
        function plusSlides(n) {
            showSlides(slideIndex += n);
        }

        // Thumbnail image controls
        function currentSlide(n) {
            showSlides(slideIndex = n);
        }

        function showSlides(n) {
            var i;
            var slides = document.getElementsByClassName("mySlides");
            var dots = document.getElementsByClassName("demo");
            var captionText = document.getElementById("caption");
            if (n > slides.length) {slideIndex = 1}
            if (n < 1) {slideIndex = slides.length}
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }
            for (i = 0; i < dots.length; i++) {
                dots[i].className = dots[i].className.replace(" active", "");
            }
            slides[slideIndex-1].style.display = "block";
            dots[slideIndex-1].className += " active";
            captionText.innerHTML = dots[slideIndex-1].alt;
        }

        document.onkeydown = nextImage;
        function nextImage(e) {

            e = e || window.event;

            if (e.keyCode == '37') {
                plusSlides(-1)
            }
            else if (e.keyCode == '39') {
                plusSlides(1)
            }

        }

    </script>
@endsection