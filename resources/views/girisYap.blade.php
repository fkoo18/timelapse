<!DOCTYPE html>
<html lang="tr" style="background: #d2d6de;">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>TimeLapse Sistem Girişi</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="/panel/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/panel/bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/panel/bower_components/Ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="/panel/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="/panel/plugins/iCheck/square/blue.css">
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="{{ route('panel.girisYap') }}"><b>Time</b>Lapse</a>
    </div>
    <!-- /.login-logo -->

    <div class="login-box-body">
        <p class="login-box-msg">Sistem Girişi</p>
        @if(session()->has('mesaj'))
            <div class="alert alert-{{ session('mesaj_tur') }}"> {{ session('mesaj') }} </div>
        @endif
        <form action="{{ route('panel.girisYap') }}" method="post">
            {{ csrf_field() }}
            <div class="form-group has-feedback">
                <input type="email" name="email" class="form-control" placeholder="Email">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="sifre" class="form-control" placeholder="Şifre">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>

                        </label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Giriş Yap</button>
                </div>
                <!-- /.col -->
            </div>
        </form>
    </div>
</div>


<script src="/panel/bower_components/jquery/dist/jquery.min.js"></script>
<script src="/panel/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/panel/plugins/iCheck/icheck.min.js"></script>
<script src="/panel/js/bizim.js"></script>
<script>
    jQuery(function ($) {
        setTimeout(function () {
            $('.alert').slideUp(500);
        }, 1800);
    });

    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' /* optional */
        });
    });
</script>
</body>
</html>
