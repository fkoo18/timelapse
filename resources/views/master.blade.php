<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield("title",config("app.name"))</title>
    @include("layouts.head")
    @yield("head")
</head>
<body class="hold-transition skin-blue sidebar-mini">

<div class="wrapper">

    @include('layouts.header')

    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="/panel/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>{{ $kullanici->email }}</p>
                    <p><small>{{ date("d/m/Y  H:i:s", strtotime($kullanici->created_at)) }}</small></p>
                </div>
            </div>

            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">TimeLapse Menü</li>
                <li {!! request()->segment(1) == 'anasayfa' ? 'class="active"' : '' !!}>
                    <a href="/">
                        <i class="fa fa-home"></i> <span>Anasayfa</span>
                    </a>
                </li>
                {{--<li class="treeview">--}}
                    {{--<a href="#">--}}
                        {{--<i class="fa fa-dashboard"></i> <span>Dashboard</span>--}}
                        {{--<span class="pull-right-container">--}}
                          {{--<i class="fa fa-angle-left pull-right"></i>--}}
                        {{--</span>--}}
                    {{--</a>--}}
                    {{--<ul class="treeview-menu">--}}
                        {{--<li class="active"><a href="index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>--}}
                        {{--<li><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <div class="content-wrapper">

        @yield("headerBaslik")

        <section class="content">

            @yield("content")

        </section>
        <!-- /.content -->
    </div>

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; 2019</strong> Tüm Hakları Saklıdır.
    </footer>


    <div class="control-sidebar-bg"></div>

</div>

@include("layouts.footer")
@yield("footer")
</body>
</html>
