-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Anamakine: 127.0.0.1
-- Üretim Zamanı: 23 May 2019, 10:39:10
-- Sunucu sürümü: 10.1.38-MariaDB
-- PHP Sürümü: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `timelapse`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `device`
--

CREATE TABLE `device` (
  `id` int(11) UNSIGNED NOT NULL,
  `hashKey` varchar(191) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `onlineStatus` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `device`
--

INSERT INTO `device` (`id`, `hashKey`, `createdDate`, `onlineStatus`) VALUES
(1, '$2y$10$F9z1wIRAUnvxcS1dli/MKektGH0crILtwlNBmegr.I0VcC1uEeZli', '2019-04-14 21:50:34', 0),
(2, '$2y$10$h180.nOcnciljNy3VqXCiOfHxxaszei0JuXC6emhAtHdJKo6M/cvu', '2019-01-23 21:00:00', 0);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `devicelog`
--

CREATE TABLE `devicelog` (
  `id` int(11) UNSIGNED NOT NULL,
  `logID` int(11) UNSIGNED NOT NULL,
  `deviceID` int(11) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `deviceproperties`
--

CREATE TABLE `deviceproperties` (
  `id` int(10) UNSIGNED NOT NULL,
  `deviceID` int(10) UNSIGNED NOT NULL,
  `propertiesID` int(10) UNSIGNED NOT NULL,
  `propertiesValue` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `deviceproperties`
--

INSERT INTO `deviceproperties` (`id`, `deviceID`, `propertiesID`, `propertiesValue`) VALUES
(1, 1, 1, '1'),
(2, 1, 2, '45000'),
(3, 1, 3, '248e5162fa84afba84cdaa371185a242');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `logtypes`
--

CREATE TABLE `logtypes` (
  `id` int(10) UNSIGNED NOT NULL,
  `logDetail` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `logtypes`
--

INSERT INTO `logtypes` (`id`, `logDetail`) VALUES
(1, 'Cihaz Açıldı'),
(2, 'Cihaz Kapandı'),
(3, 'Resim Yüklenemedi'),
(4, 'FTP Ulaşılamadı'),
(5, 'Servis Modunda işlem yapılıyor..'),
(6, 'FTP Bağlandı.'),
(7, 'Resim Yüklendi'),
(8, 'Fotoğraf Çekme Zamanı Değiştirildi');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `owneddevices`
--

CREATE TABLE `owneddevices` (
  `id` int(10) UNSIGNED NOT NULL,
  `userID` int(10) UNSIGNED NOT NULL,
  `deviceID` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `owneddevices`
--

INSERT INTO `owneddevices` (`id`, `userID`, `deviceID`) VALUES
(1, 1, 1),
(2, 1, 2);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `photo`
--

CREATE TABLE `photo` (
  `id` int(10) UNSIGNED NOT NULL,
  `photoName` varchar(34) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deviceID` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `propertiestype`
--

CREATE TABLE `propertiestype` (
  `id` int(10) UNSIGNED NOT NULL,
  `properties` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `propertiestype`
--

INSERT INTO `propertiestype` (`id`, `properties`) VALUES
(1, 'uploadAfterPowerOff'),
(2, 'shutterTime'),
(3, 'imagePath');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(191) NOT NULL,
  `api_token` varchar(60) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `api_token`, `status`, `created_at`, `updated_at`, `remember_token`) VALUES
(1, 'furkan@gmail.com', '$2y$10$Pw084cCsOG80Wh2pWc2ajeYfVCyCGWyDe8DU.0wOGkVGQCkNg6hXC', '8nKViEx4haYWEqF0E505TFSGCHJYTVLQXOurlSrDoBuM9TFIAmkjAyTP8piI', 1, '2019-01-24 10:13:13', '2019-01-24 10:13:13', 'rZumQKWNzCEAwecUOgA5YC6ajCnmeHAeOo1drEdfjYIQB0WaJ1J5AFPExV4j'),
(3, 'melih@gmail.com', '$2y$10$Pw084cCsOG80Wh2pWc2ajeYfVCyCGWyDe8DU.0wOGkVGQCkNg6hXC', '9nKViEx4haYWEqF0E505TFSGCHJYTVLQXOurlSrDoBuM9TFIAmkjAyTP8piI', 1, '2019-01-30 10:13:13', '2019-01-30 10:13:13', NULL);

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `device`
--
ALTER TABLE `device`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `devicelog`
--
ALTER TABLE `devicelog`
  ADD PRIMARY KEY (`id`),
  ADD KEY `devideID` (`deviceID`),
  ADD KEY `logID` (`logID`);

--
-- Tablo için indeksler `deviceproperties`
--
ALTER TABLE `deviceproperties`
  ADD PRIMARY KEY (`id`),
  ADD KEY `deviceID` (`deviceID`),
  ADD KEY `propertiesID` (`propertiesID`);

--
-- Tablo için indeksler `logtypes`
--
ALTER TABLE `logtypes`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `owneddevices`
--
ALTER TABLE `owneddevices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `deviceID` (`deviceID`),
  ADD KEY `accountID` (`userID`);

--
-- Tablo için indeksler `photo`
--
ALTER TABLE `photo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `deviceID` (`deviceID`),
  ADD KEY `deviceID_2` (`deviceID`);

--
-- Tablo için indeksler `propertiestype`
--
ALTER TABLE `propertiestype`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `api_token` (`api_token`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `device`
--
ALTER TABLE `device`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Tablo için AUTO_INCREMENT değeri `devicelog`
--
ALTER TABLE `devicelog`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `deviceproperties`
--
ALTER TABLE `deviceproperties`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Tablo için AUTO_INCREMENT değeri `logtypes`
--
ALTER TABLE `logtypes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Tablo için AUTO_INCREMENT değeri `owneddevices`
--
ALTER TABLE `owneddevices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Tablo için AUTO_INCREMENT değeri `photo`
--
ALTER TABLE `photo`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `propertiestype`
--
ALTER TABLE `propertiestype`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Tablo için AUTO_INCREMENT değeri `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Dökümü yapılmış tablolar için kısıtlamalar
--

--
-- Tablo kısıtlamaları `devicelog`
--
ALTER TABLE `devicelog`
  ADD CONSTRAINT `devicelog_ibfk_1` FOREIGN KEY (`logID`) REFERENCES `logtypes` (`id`),
  ADD CONSTRAINT `devicelog_ibfk_2` FOREIGN KEY (`deviceID`) REFERENCES `device` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `deviceproperties`
--
ALTER TABLE `deviceproperties`
  ADD CONSTRAINT `deviceproperties_ibfk_2` FOREIGN KEY (`propertiesID`) REFERENCES `propertiestype` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `deviceproperties_ibfk_3` FOREIGN KEY (`deviceID`) REFERENCES `device` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `owneddevices`
--
ALTER TABLE `owneddevices`
  ADD CONSTRAINT `owneddevices_ibfk_2` FOREIGN KEY (`userID`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `owneddevices_ibfk_3` FOREIGN KEY (`deviceID`) REFERENCES `device` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `photo`
--
ALTER TABLE `photo`
  ADD CONSTRAINT `photo_ibfk_1` FOREIGN KEY (`deviceID`) REFERENCES `device` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
