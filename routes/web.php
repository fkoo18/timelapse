<?php

use Illuminate\Support\Facades\Route;


Route::match(['get','post'], '/', 'Panel\KullaniciController@girisYap')->name('panel.girisYap');

Route::group(['middleware' => 'panel'], function() {

    Route::get('/anasayfa', 'Panel\AnasayfaController@index')->name('panel.anasayfa');
    Route::get('/devicePhotos', 'Panel\PhotoController@index')->name('panel.devicePhotos');
    Route::post('/logBilgiGetir', 'Panel\AnasayfaController@logBilgiGetir')->name('panel.logBilgiGetir');
    Route::get('/cikis', 'Panel\KullaniciController@cikis')->name('panel.cikis');

});