<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::match(['get','post'], '/login', 'Api\UserController@login');


    Route::group(['prefix' => 'device'], function (){
        Route::match(['get','post'], '/getAll', 'Api\DeviceController@getAll');
        Route::match(['get','post'], '/onlineStatusUpdate', 'Api\DeviceController@onlineStatusUpdate');
        Route::match(['get','post'], '/getDeviceProperties', 'Api\DevicePropertiesController@getDeviceProperties');
//        Route::match(['get','post'], '/getDeviceLog', 'Api\DeviceLogController@getDeviceLog');
        Route::match(['get','post'], '/insertDeviceLog', 'Api\DeviceLogController@insertDeviceLog');
    });


Route::match(['get','post'], '/device/properties/all', 'Api\DeviceController@onlineStatusUpdate');


Route::match(['get','post'], '/photo/getAll', 'Api\PhotoController@getAll');
Route::match(['get','post'], '/photo/insertData', 'Api\PhotoController@insertData');




//Route::group(['middleware' => 'auth:api'], function() {
//
//
//
//
//    Route::group(['prefix' => 'user'], function (){
//        Route::get('/', 'Api\UserController@user');
//    });
//});

//Route::get('/login', 'Api\userController@login');