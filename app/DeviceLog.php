<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeviceLog extends Model
{

    public $timestamps = false;

    protected $table = 'devicelog';

    protected $guarded = [];

    public function deviceLogName()
    {
        return $this->hasOne('App\LogTypes', 'logID', 'id')->withDefault();
    }

}
