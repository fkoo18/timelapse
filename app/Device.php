<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{

    public $timestamps = false;

    protected $table = 'device';

    protected $guarded = [];

    public function deviceLogAcik() /// 1 Veritabanında Açık...
    {
        return $this->hasMany('App\DeviceLog', 'deviceID')->where('logID', 1)->orderBy('created_at','desc')->take(1);
    }

    public function deviceLogKapali() /// 2 Veritabanında Kapalı...
    {
        return $this->hasMany('App\DeviceLog', 'deviceID')->where('logID', 2)->orderBy('created_at','desc')->take(1);
    }

    public function deviceLog()
    {
        return $this->hasMany('App\DeviceLog', 'deviceID')->orderBy('created_at','desc')->take(10);
    }

}
