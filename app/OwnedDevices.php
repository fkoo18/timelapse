<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OwnedDevices extends Model
{

    public $timestamps = false;

    protected $table = 'owneddevices';

    protected $guarded = [];

    public function device()
    {
        return $this->hasOne('App\Device', 'id')->withDefault();
    }
}
