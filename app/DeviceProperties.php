<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeviceProperties extends Model
{

    public $timestamps = false;

    protected $table = 'deviceproperties';

    protected $guarded = [];

    public function ozellik()
    {
        return $this->belongsToMany('App\PropertiesType', 'id', 'propertiesID');
    }

}
