<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogTypes extends Model
{

    public $timestamps = false;

    protected $table = 'logtypes';

    protected $guarded = [];

}
