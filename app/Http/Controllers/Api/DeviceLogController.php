<?php

namespace App\Http\Controllers\Api;
use App\Device;
use App\DeviceLog;
use App\DeviceProperties;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;

class DeviceLogController extends Controller
{

    /**
     ** Api Link     :  /api/device/getDeviceLog
     ** Verilen Data :  Api - deviceID
     **/
//    public function getDeviceLog(Request $request)
//    {
//        if(request()->isMethod('get')){
//            $user = User::where('api_token', $request->api)->first();
//            if ($user) {
//
//                $deviceProperties = DB::select("
//                    SELECT dl.id, lt.logDetail, dl.deviceID, dl.created_at
//                    FROM devicelog dl
//                    INNER JOIN logtypes lt ON lt.id = dl.logID
//                    ORDER BY id DESC
//                    WHERE dl.deviceID = " . $request->deviceID
//                );
//
//                return response()->json([
//                    'deviceProperties'   => $deviceProperties,
//                    'status'             => 200
//                ],200);
//            }
//            return response()->json([
//                'mesaj' => 'Api Token Hatalı',
//                'status' => 401,
//            ],401);
//        }
//        return response()->json([
//            'mesaj' => 'Geçersiz İstek'
//        ],401);
//    }

    /**
     ** Api Link     :  /api/device/insertDeviceLog
     ** Verilen Data :  Api - logID - deviceID
     **/
    public function insertDeviceLog(Request $request)
    {
        if(request()->isMethod('post')){
            $user = User::where('api_token', $request->api)->first();
            if ($user) {

                $data = $request->only('logID', 'deviceID');
                $result = DB::table('devicelog')->insert($data);

                if($result){
                    return response()->json([
                        'result'   => 'Kayıt Eklendi',
                        'status'   => 200
                    ],200);
                }else{
                    return response()->json([
                        'result'   => 'Kayıt Eklenemedi',
                        'status'   => 401
                    ],401);
                }

            }
            return response()->json([
                'mesaj' => 'Api Token Hatalı',
                'status' => 401,
            ],401);
        }
        return response()->json([
            'mesaj' => 'Geçersiz İstek'
        ],401);
    }

}
