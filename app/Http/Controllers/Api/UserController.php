<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class userController extends Controller
{
    /**
     ** Api Link     :  /api/login
     ** Verilen Data :  Api
     **/
    public function login(Request $request)
    {
        if(request()->isMethod('POST')){
            $user = User::where('email', $request->email)->first();
            if ($user && Hash::check($request->password, $user->password)) {

//                geri:
//                $apiKey = ['api_token' => str_random(60)];
//
//                $validator = Validator::make($apiKey, [
//                    'api_token'           => "unique:users,api_token"
//                ]);
//                if ($validator->fails()) {
//                    goto geri;
//                }
//
//                $user->api_token = $apiKey['api_token'];
//                $user->save();

                return response()->json([
                    'api_token' => $user->api_token,
                    'email' => $user->email,
                    'status' => 200,
                    'id' => $user->id
                ],200);
            }
            return response()->json([
                'mesaj' => 'Kullanıcı adı veya Şifre Hatalı.',
                'status' => 401,
            ],401);
        }
        return response()->json([
            'mesaj' => 'Geçersiz İstek',
            'status' => 401,
        ],401);
    }

}
