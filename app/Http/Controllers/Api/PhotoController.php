<?php

namespace App\Http\Controllers\Api;
use App\DeviceProperties;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Photo;
use App\User;

class PhotoController extends Controller
{
    /**
     ** Api Link     :  /api/photo/getAll
     ** Verilen Data :  Api - deviceID
     **/
    public function getAll(Request $request)
    {
        if(request()->isMethod('post')){
            $user = User::where('api_token', $request->api)->first();
            if ($user) {
                $photo      = Photo::where('deviceID', $request->deviceID)->take(10)->get();
                $devicePath = DeviceProperties::where('deviceID', $request->deviceID)->where('propertiesID', 3)->get();
                return response()->json([
                    'photo'       => $photo,
                    'devicePath'  => $devicePath[0]->propertiesValue
                ],200);
            }
            return response()->json([
                'mesaj' => 'Api Token Hatalı',
                'status' => 401,
            ],401);
        }
        return response()->json([
            'mesaj' => 'Geçersiz İstek'
        ],401);
    }

    /**
     ** Api Link     :  /api/photo/insertData
     ** Verilen Data :  api - photoName - date - deviceID
     **/
    public function insertData(Request $request)
    {
        if(request()->isMethod('POST')){
            $user = User::where('api_token', $request->api)->first();
            if ($user) {
                $photo = new Photo;
                $photo->photoName  =  $request->photoName;
                $photo->date       =  $request->date;
                $photo->deviceID   =  $request->deviceID;
                $photo->save();

                return response()->json([
                    'status'  => 200
                ],200);
            }
            return response()->json([
                'mesaj' => 'Api Token Hatalı',
                'status' => 401,
            ],401);
        }
        return response()->json([
            'mesaj' => 'Geçersiz İstek'
        ],401);
    }


}
