<?php

namespace App\Http\Controllers\Api;
use App\Device;
use App\Http\Controllers\Controller;
use App\OwnedDevices;
use Illuminate\Http\Request;
use App\User;

class DeviceController extends Controller
{

    /**
     ** Api Link     :  /api/device/getAll
     ** Verilen Data :  Api
     **/
    public function getAll(Request $request)  // Api değerindeki kullanıcıya ait deviceleri verir.
    {
        if(request()->isMethod('post')){
            $user = User::where('api_token', $request->api)->first();
            if ($user) {
                $ownedDevice = OwnedDevices::where('userID', $user->id)->get();
                $device = array();
                foreach ($ownedDevice as $data){
                    $veri = Device::where('id', $data->deviceID)->get();
                    array_push($device, $veri[0]);
                }
                return response()->json([
                    'devices'   => $device
                ],200);
            }
            return response()->json([
                'mesaj' => 'Api Token Hatalı',
                'status' => 401,
            ],401);
        }
        return response()->json([
            'mesaj' => 'Geçersiz İstek'
        ],401);
    }


    /**
    ** Api Link     :  /api/device/onlineStatusUpdate
    ** Verilen Data :  Api - deviceID - onlineStatusData
    **/
    public function onlineStatusUpdate(Request $request)
    {
        if(request()->isMethod('post')){
            $user = User::where('api_token', $request->api)->first();
            if ($user) {

                $device = Device::find($request->deviceID);

                $device->onlineStatus = $request->onlineStatusData;
                $device->save();

                return response()->json([
                    'device'   => $device,
                    'status'   => 200
                ],200);
            }
            return response()->json([
                'mesaj' => 'Api Token Hatalı',
                'status' => 401,
            ],401);
        }
        return response()->json([
            'mesaj' => 'Geçersiz İstek'
        ],401);
    }


}
