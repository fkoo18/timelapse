<?php

namespace App\Http\Controllers\Api;
use App\Device;
use App\DeviceProperties;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;

class DevicePropertiesController extends Controller
{

    /**
     ** Api Link     :  /api/device/getDeviceProperties
     ** Verilen Data :  Api - deviceID
     **/
    public function getDeviceProperties(Request $request)
    {
        if(request()->isMethod('post')){
            $user = User::where('api_token', $request->api)->first();
            if ($user) {

                $deviceProperties = DB::select("
                    SELECT dp.id, dp.deviceID, pt.properties, dp.propertiesValue
                    FROM deviceproperties dp
                    INNER JOIN propertiestype pt ON pt.id = dp.propertiesID
                    WHERE dp.deviceID = " . $request->deviceID
                );

                foreach ($deviceProperties as $data){
                    $device[$data->properties] = $data->propertiesValue;
                }

                // Bunun gibi veri dönüyor.
                // "uploadAfterPowerOff": "1"
                // "shutterTime": "800"

                return response()->json([
                    'deviceProperties'   => $device,
                    'status'             => 200
                ],200);
            }
            return response()->json([
                'mesaj' => 'Api Token Hatalı',
                'status' => 401,
            ],401);
        }
        return response()->json([
            'mesaj' => 'Geçersiz İstek'
        ],401);
    }

}
