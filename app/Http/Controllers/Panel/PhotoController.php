<?php

namespace App\Http\Controllers\Panel;

use App\Device;
use App\OwnedDevices;
use App\Photo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PhotoController extends Controller
{

    public function index(Request $request)
    {
        $device = Device::find(request('deviceID'));
        $od     = OwnedDevices::find(request('deviceID'));
        if($device == null || auth('panel')->id() != $od->userID){
            return back();
        }

        if (!request('baslangicTarih') && !request('bitisTarih')) {
            $devicePhoto = Photo::where('deviceID', request('deviceID'))
                ->whereRaw('date BETWEEN "' . substr(htmlspecialchars(date("Y-m-d H:i:s")), 0, 10) . ' 00:00:00" AND "' . substr(htmlspecialchars(date("Y-m-d H:i:s")), 0, 10) . ' 23:59:59"')
                ->orderByDesc('date')
                ->paginate(1500);
        }
        else{
            $devicePhoto = Photo::where('deviceID', request('deviceID'))
                ->whereRaw('date BETWEEN "'.htmlspecialchars(request('baslangicTarih')).' 00:00:00" AND "'.htmlspecialchars(request('bitisTarih')).' 23:59:59"')
                ->orderByDesc('date')
                ->paginate(1500);
        }

        return view('devicePhotos', compact('device','devicePhoto'));
    }

}
