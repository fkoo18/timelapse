<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class KullaniciController extends Controller
{

    public function girisYap(Request $request)
    {
        if (auth('panel')->id()){
            return redirect()->route('panel.anasayfa');
        }
        if (request()->isMethod('POST'))
        {
            $this->validate(request(),[
                'email' => 'required|email',
                'sifre' => 'required'
            ]);

            $credentials = [
                'email'       => request('email'),
                'password'    => request('sifre')
            ];

            if(Auth::guard('panel')->attempt($credentials, false))
            {
                request()->session()->regenerate();

                return redirect()->route('panel.anasayfa');
            }
            else
            {
                return back()->withInput()->with(['mesaj_tur' => 'error', 'mesaj' => 'Kullanıcı Adı veya Şifre Hatalı']);
            }
        }
        return view('girisYap');

    }

    public function cikis()
    {
        Auth::guard('panel')->logout();
        request()->session()->forget('panel');
        request()->session()->regenerate();
        return redirect()->route('panel.girisYap');
    }


}
