<?php

namespace App\Http\Controllers\Panel;

use App\Device;
use App\DeviceLog;
use App\OwnedDevices;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Request;
use Illuminate\Support\Facades\DB;

class AnasayfaController extends Controller
{

    public function index()
    {
        $ownedDevice = OwnedDevices::with('device')->where('userID', auth('panel')->id())->get();
        return view('anasayfa', compact('ownedDevice'));
    }

    public function logBilgiGetir(Request $request)
    {
        $logs = DB::table('devicelog')
            ->select('logtypes.logDetail', 'devicelog.created_at')
            ->join('logtypes', 'logtypes.id', '=', 'devicelog.logID')
            ->where('devicelog.deviceID', request('id'))
            ->orderByDesc('devicelog.id')
            ->paginate(10);
        return $logs;
    }

}
