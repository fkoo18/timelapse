<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Panel
{
    public function handle($request, Closure $next)
    {
        if (Auth::guard('panel')->check())
        {
            return $next($request);
        }

        return redirect()->route('panel.girisYap');

    }
}
