<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertiesType  extends Model
{

    public $timestamps = false;

    protected $table = 'propertiestype';

    protected $guarded = [];

}
